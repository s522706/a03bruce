var http = require('http');
var fs = require("fs");
var express = require('express');
var app = express();
var path = require("path");
var logger = require("morgan");
var bodyParser = require("body-parser");

app.set('view engine', 'ejs');
app.use('/assets', express.static('assets'));


app.get('/main', function(request,response){
  response.sendFile(__dirname + '/assets/Main.html');
});

app.get('/contact', function(request,response){
  response.sendFile(__dirname + '/assets/Contact.html');
});

app.get('/extra', function(request,response){
  response.sendFile(__dirname + '/assets/Extra.html');
});

app.get('/guestbook', function(request,response){
  response.render('index');
});

app.get('/', function(request,response){
  response.render('index');
});

var entries = [];
app.locals.entries = entries;

app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/");
});

app.use(function (request, response) {
  response.status(404).render("404");
});

app.listen(3000);
